package bookstoreProject.main.model;

import java.text.DecimalFormat;

public class Book {
    private String title;
    private String author;
    private int serialNumber;
    private double price;

    public Book(String title, String author, int serialNumber, double price) {
        this.title = title;
        this.author = author;
        this.serialNumber = serialNumber;
        this.price = price;
    }

    private static final DecimalFormat df = new DecimalFormat("0.00");

    public int getSerialNumber() {
        return serialNumber;
    }

    public void printDetails() {
        printDetails(false);
    }

    public void printDetails(Boolean full) {
        if(full) {
            System.out.println(toString());
        } else {
            System.out.println("Title: " + this.title + "\nAuthor: " + this.author + "\nSerial number: " + this.serialNumber + "\n");
        }

    }

    @Override
    public String toString() {
        return "Title: " + this.title + "\nAuthor: " + this.author + "\nSerial number: " + this.serialNumber + "\nPrice: " + df.format(this.price) + "\n";
    }
}
