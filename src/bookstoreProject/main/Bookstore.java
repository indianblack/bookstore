package bookstoreProject.main;

import bookstoreProject.main.model.Book;

import java.text.DecimalFormat;
import java.util.*;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class Bookstore {
    private ArrayList<Book> stock;

    public Bookstore(ArrayList<Book> stock) {
        this.stock = stock;
    }

    private static final DecimalFormat df = new DecimalFormat("0.00");

    public void printMainMenu() {
        System.out.println( "Back to main menu.\n" +
                "Menu \n" +
                "0 - exit \n" +
                "1 - borrow a book \n" +
                "2 - buy a book \n" +
                "3 - add a book \n");
        System.out.println("Please choose one of the actions above: \n");
    }

    public void run() {
        // Using Scanner to get input from user
        Scanner input = new Scanner(System.in);

        // User input
        int userInput = -1;

        // Bookstore rental menu
        System.out.println("Hello user! \n" +
                "Menu \n" +
                "0 - exit \n" +
                "1 - borrow a book \n" +
                "2 - buy a book \n" +
                "3 - add a book \n");
        System.out.println("Please provide one of the actions above: \n");

        while (userInput != 0) {
            try {
                userInput = input.nextInt();

                switch (userInput) {
                    case 0:
                        System.out.println("Bye");
                        break;
                    case 1:
                        borrowBook();
                        break;
                    case 2:
                        buyBook();
                        break;
                    case 3:
                        addBook();
                        break;
                    default:
                        System.out.println("Option " + userInput + " is not available. " + "Please choose one of the following: \n" +
                                "0 - exit \n" +
                                "1 - borrow a book \n" +
                                "2 - buy a book \n" +
                                "3 - add a book \n");
                }
            } catch (Exception exception){
                System.out.println("Invalid input. Please provide a number: \n");
                input.nextInt();
            }
        }
    }

    public void printAvailableBooks(Boolean full) {
        if (stock.size() == 0) {
            System.out.println("No books available.\n");
        } else if (full){
            System.out.println("Available books:\n");
            stock.forEach(it -> it.printDetails(true));
        } else {
            stock.forEach(it -> it.printDetails());
        }
    }

    public void borrowBook() {
        System.out.println("Borrow a book \n");

        // Show available books
        printAvailableBooks(false);

        // Get serial number of a book
        Scanner bookInput = new Scanner(System.in);

        try {
            System.out.println("Provide the serial number of book you want to borrow: ");
            int bookSerialNumber = bookInput.nextInt();

            // Check if the serial number is correct
            Optional<Book> findBook = stock.stream().filter(it -> it.getSerialNumber() == bookSerialNumber).findAny();

            if(findBook.isPresent()) {
                // If serial number is correct - remove book from stock
                Collection<Book> newStock = stock.stream()
                        .filter(it -> it.getSerialNumber() != bookSerialNumber)
                        .collect(Collectors.toList());

                ArrayList<Book> newStockArray = new ArrayList<>(newStock);
                this.stock = newStockArray;

                // Inform the user he just bought a book
                System.out.println("You just borrowed a book!");
                findBook.get().printDetails();
            }
        } catch (Exception exception){
            System.out.println("Book with provided serial number doesn't exist.\n");
        }

        // Back to main menu
        printMainMenu();
    }

    public void buyBook() {
        System.out.println("Buy a book \n");

        // Show available books
        printAvailableBooks(true);

        // Get serial number of a book
        Scanner bookInput = new Scanner(System.in);

        try {
            System.out.println("Provide the serial number of book you want to buy: ");
            int bookSerialNumber = bookInput.nextInt();

            // Check if the serial number is correct
            Optional<Book> findBook = stock.stream().filter(it -> it.getSerialNumber() == bookSerialNumber).findAny();

            if(findBook.isPresent()) {
                // If serial number is correct - remove book from stock
                Collection<Book> newStock = stock.stream()
                        .filter(it -> it.getSerialNumber() != bookSerialNumber)
                        .collect(Collectors.toList());

                ArrayList<Book> newStockArray = new ArrayList<>(newStock);
                this.stock = newStockArray;

                // Inform the user he just bought a book
                System.out.println("You just bought a book!");
                findBook.get().printDetails(true);
            }
        } catch (Exception exception){
            System.out.println("Book with provided serial number doesn't exist.\n");
        }

        // Back to main menu
        printMainMenu();
    }

    public void addBook() {
        System.out.println("Add a book to bookstore \n");
        System.out.println("To add a book, provide the necessary information...  \n");

        // Get serial number of a book
        Scanner input = new Scanner(System.in);

        // Get title
        System.out.println("Title: ");
        String bookTitle = input.nextLine();

        // Check if title is not empty
        while(bookTitle.isBlank()) {
            System.out.println("No title provided. Please provide a title.\n");
            bookTitle = input.nextLine();
        }

        // Get author
        System.out.println("Author: ");
        String bookAuthor = input.nextLine();

        while(bookAuthor.isBlank()) {
            System.out.println("No author provided. Please provide an author.\n");
            bookAuthor = input.nextLine();
        }

        // Generate serial number
        int serialNumber;
        Optional<Book> checkSerialNumber;

        // Check if the serial number already exists
        do {
            serialNumber = Math.abs(new Random().nextInt());
            Optional<Book> found = Optional.empty();
            for (Book it : stock) {
                if (it.getSerialNumber() == serialNumber) {
                    found = Optional.of(it);
                    break;
                }
            }
            checkSerialNumber = found;
        } while (checkSerialNumber.isPresent());


        // Generate price
        float price = new Random().nextFloat() * 100;

        // Add book to stock
        Book providedBook = new Book(bookTitle, bookAuthor, serialNumber, price);
        stock.add(providedBook);

        System.out.println("Book added:\n");
        providedBook.printDetails(true);

        // Back to main menu
        printMainMenu();
    }
}
