package bookstoreProject.main;

import bookstoreProject.main.model.Book;

import java.util.ArrayList;

public class Main {
    private static ArrayList<Book> createBookstore() {
        Book lostTime = new Book("In Search of Lost Time","Marcel Proust", 1, 52.99 );
        Book ulysses = new Book("Ulysses","James Joyce", 2, 70.00 );
        Book donQuixote = new Book("Don Quixote","Miguel de Cervantes", 3, 30.00 );
        Book prideAndPrejudice  = new Book("Pride and Prejudice","Jane Austen", 4, 35.99 );
        Book killMockingbird   = new Book("To Kill a Mockingbird","Harper Lee", 5, 32.99 );
        ArrayList<Book> booksCollection = new ArrayList<>();
        booksCollection.add(lostTime);
        booksCollection.add(ulysses);
        booksCollection.add(donQuixote);
        booksCollection.add(prideAndPrejudice);
        booksCollection.add(killMockingbird);
        return booksCollection;
    }

    public static void main(String[] args) {
        // Create bookstore
        ArrayList<Book> bookstoreStock = createBookstore();
        Bookstore localBookstore = new Bookstore(bookstoreStock);

        // Run bookstore
        localBookstore.run();
    }
}
