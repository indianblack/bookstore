# Simple bookstore

Java application that handles a simple bookstore.

## Basic requirements
1. The program should be able to store information about a book (title, author, serial number, price). To store the books data choose any collection you know.
2. At the beginning, the program should greet the user and ask him if he wants to: borrow, buy or add the book to the bookstore.
3. If the user wants to add it to the bookstore, the program should get the necessary data from the user and save the book.
4. If the user wants to buy a book, the program should display all available books, then the user selects the book and the program should remove the book from the database.
5. Finally, the program should ask the user if he wants to do something else, if not - terminate the program.

## Instructions
1. Clone this repository.
2. Run project in IDE.

## Useful links
- How to push to GitLab: https://zapier.com/blog/how-to-push-to-gitlab/
- 100 must-read classic: https://www.penguin.co.uk/articles/2018/100-must-read-classic-books.html
- Get ArrayList from stream: https://www.geeksforgeeks.org/how-to-get-arraylist-from-stream-in-java-8/
- How to round double/float value: https://mkyong.com/java/how-to-round-double-float-value-to-2-decimal-points-in-java/#decimalformat000
- Java do while loop: https://www.journaldev.com/16536/java-do-while-loop

## License
[MIT](https://choosealicense.com/licenses/mit/)